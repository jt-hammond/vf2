$(function() {
	// Length Needs to Correspond to days and Vice Versa
	// length needs to equal days(food, accomoadation, activities and car)
	let $length = $('#travelTime');

	// Needs to Save Necessary Info and add it to the screen to main screen
	let $goalName = $('#goalName');
	let $destination = $('#destinationName');
	let $type = $('#travelType');

	// Needs to move between Modals and titles
	// Needs to advance progress bar
	let $whereTitle = $('#whereTitle');
	let $costTitle = $('#costTitle');
	let $summaryTitle = $('#summaryTitle');

	let $whereBody = $('#whereBody');
	let $costBody = $('#costBody');
	let $summaryBody = $('#summaryBody');

	$("#modalProgress").css("width", "66%");
	$("#modalProgress").css("width", "100%");

	// Needs to calculate amounts for estimates
	let $amountFlights = $('#amountFlights');
	let $people = $('#peopleFlights');
	let $estimateFlight = $('#estimateFlight');
	
	let $amountAccomodations = $('#amountAccomodations');
	let $daysAccomodations = $('#daysAccomodations');
	let $estimateAccomadation = $('#estimateAccomadation');

	let $amountCar = $('#amountCar');
	let $daysCar = $('#daysCar');
	let $estimateCar = $('#estimateCar');

	let $amountFood = $('#amountFood');
	let $daysFood = $('#daysFood');
	let $estimateFood = $('#estimateFood');

	let $amountActivities = $('#amountActivities');
	let $daysActivities = $('#daysActivities');
	let $estimateActivities = $('#estimateActivities');
	
	// Needs to calculate total amount 
	// total = estimate(flight, activities, car, accomodates, food)
	let $total = $('#total');

	// Needs to use estimate if checked
	// If checked than total is equal to the estimate and estimateCost
	let $estimateGeneral = $('#estimateGeneral');
	let $guessEstimate = $('#guessEstimate');


	// Needs to replace elements in summary page
	let $image = $('#fileUpload');
	let $estimateCost = $('#costEstimate');
	let $date = $('#date');
	let $contribution = $('#contribution');
	let $final = $('#final');


	// Needs to append to the main body
	const $mainBody = $('#main-body');
});